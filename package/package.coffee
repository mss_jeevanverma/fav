Package.describe summary: "Fav Package"
Package.on_use (api) ->
  api.use ["standard-app-packages"], [
    "client"
    "server"
  ]
  api.use ["iron-router"], "client"
  api.add_files [
    "client/index.html"
    "client/routes.js"
  ], "client"
  return