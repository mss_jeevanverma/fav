# Write your package code here!
# Set up a collection to contain doc information. On the server,
# it is backed by a MongoDB collection named "docs".
fav = new Meteor.Collection("fav")
if Meteor.isClient
  Session.setDefault "sort",
    score: -1
    name: 1

  Template.fav.docs = ->
    fav.find {},
      sort: Session.get("sort")


  
  # Descirbes in English how the list is sorted 
  Template.fav.sort = ->
    sort = Session.get("sort")
    output = "Sorting by "
    _.each sort, (v, k, l) ->
      order = ""
      order = "ascending"  if v is 1
      order = "descending"  if v is -1
      output += k + " " + order + ", "
      return

    output

  Template.fav.selected_name = ->
    doc = fav.findOne(Session.get("selected_doc"))
    doc and doc.name

  Template.doc.selected = ->
    (if Session.equals("selected_doc", @_id) then "warning" else "")

  
  # When you press the button, add 5 points to the doc 
  Template.fav.events
    "click #increment": ->
      fav.update Session.get("selected_doc"),
        $inc:
          score: 5

      return

    "click #delete": ->
      result = confirm("Want to delete?")
      fav.remove Session.get("selected_doc")  if result
      return

    "click #insert": ->
      n = $("input[name=name]").val()
      unless n is ""
        fav.insert
          name: n
          score: 0

        $("input[name=name]").val ""
      
      # return false so the page will not reload
      false

    "click #name": ->
      sort = Session.get("sort")
      sort =
        name: sort.name * -1
        score: sort.score

      Session.set "sort", sort
      return

    "click #score": ->
      sort = Session.get("sort")
      if _.has(sort, "score")
        sort =
          score: sort.score * -1
          name: sort.name
      else
        sort =
          score: -1
          name: sort.name
      Session.set "sort", sort
      return

    "click #shuffle": ->
      
      # get all docs
      docs = fav.find({}).fetch()
      
      # clear all docs
      i = 0

      while i < docs.length
        fav.remove docs[i]._id
        i++
      
      # Re-insert docs with new scores
      i = 0

      while i < docs.length
        doc =
          name: docs[i].name
          score: Math.floor(Random.fraction() * 10) * 5

        fav.insert doc
        i++
      return

  
  # When you click a doc, select it 
  Template.doc.events click: ->
    Session.set "selected_doc", @_id
    return
